﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day2 {
    internal static class Program {
        struct Entry {
            public string Len;
            public string Chr;
            public string Pw;
            public Entry(string len, string chr, string pw) {
                this.Len = len;
                this.Chr = chr;
                this.Pw = pw;
            }
        }
        
        static void Main(string[] args) {
            var inputFile = File.ReadAllLines(args[0]);
            var inputData = new List<Entry>();
            
            foreach (var str in inputFile) {
                var line = str.Split(" ");
                inputData.Add(new Entry(
                        len: line[0],
                        chr: line[1].Replace(":", ""),
                        pw: line[2]
                        ));
            }

            Console.WriteLine($"Solving with input: \n{string.Join("\n", inputFile)}");
            Console.WriteLine($"Part 1 result: {Solve1(inputData.ToArray()).ToString()}");
            Console.WriteLine($"Part 2 result: {Solve2(inputData.ToArray()).ToString()}");
        }

        static int Solve1(Entry[] input) {
            var count = 0;
            foreach (var ent in input) {
                var minMax = ent.Len.Split("-");
                var charCount = Regex.Matches(ent.Pw, ent.Chr).Count();
                if (Convert.ToInt32(minMax[0]) <= charCount && Convert.ToInt32(minMax[1]) >= charCount) {
                    count++;
                }
            }

            return count;
        }

        static int Solve2(Entry[] input) {
            var count = 0;
            foreach (var ent in input) {
                var pos = ent.Len.Split("-");
                var matches = Regex.Matches(ent.Pw, ent.Chr);
                if (matches.Select(x => pos.Contains((x.Index + 1).ToString())).Count(n => n) == 1) {
                    Console.WriteLine($"valid pw found: {ent.Len} {ent.Chr}: {ent.Pw}");
                    count++;
                };
            }
            
            return count;
        }
    }
}