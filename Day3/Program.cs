﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Day3 {
    internal static class Program {

        static char[][] ParseInput(string inputFileName) {
            var inputFile = File.ReadAllLines(inputFileName);
            var inputData = new List<char[]>();
            foreach (var line in inputFile) {
               var row = new List<char>();
                foreach (var chr in line) {
                    row.Add(chr);
                }
                inputData.Add(row.ToArray());
            }

            return inputData.ToArray();
        }
        
        static void Main(string[] args) {
            var input = ParseInput(args[0]);
            (int, int) slope = (3, 1);
            (int, int)[] slopes = {(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)};

            Console.WriteLine($"Solving with input: \n{string.Join("", input.Select(x => new string(x) + "\n"))}");
            Console.WriteLine($"Part 1 result: {Solve1(input, slope).ToString()}");
            Console.WriteLine($"Part 2 result: {Solve2(input, slopes).ToString()}");
        }

        static int Solve1(char[][] input, (int, int) slope) {
            var count = 0;
            bool done = false;

            int x = 0;
            int y = 0;
            while (!done) {
                if (input[y][x] == '#') {
                    count++;
                } 

                x += slope.Item1;
                y += slope.Item2;

                if (y >= input.Length) {
                    done = true;
                }

                if (x >= input[0].Length) {
                    x -= input[0].Length;
                }
            }
            return count;
        }
        
        static long Solve2(char[][] input, (int, int)[] slopes) {
            long count = 1;
            foreach (var slope in slopes) {
                Console.WriteLine($"Slope ({slope.Item1.ToString()},{slope.Item2.ToString()})");
                var result = Solve1(input, slope);
                Console.WriteLine(result);
                count *= result;
            }

            return count;
        }
        
    }
}