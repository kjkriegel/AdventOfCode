﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day4 {
    internal static class Program {
        static IEnumerable<Dictionary<string, string>> ParseInput(string inputFileName) {
            var inputFile = File.ReadAllText(inputFileName);
            var input = inputFile.Split("\n\n");

            var inputData = new List<Dictionary<string, string>>();
            foreach (var ds in input) {
                var kvs = ds.Replace("\n", " ").Split(" ");
                var dsKv = new Dictionary<string, string>();
                foreach (var kvstring in kvs) {
                    var kv = kvstring.Split(":");
                    // Console.WriteLine($"{kv[0]}:{kv[1]}");
                    dsKv.Add(kv[0], kv[1]);
                }

                inputData.Add(dsKv);
            }

            return inputData;
        }

        public static bool IsWithin(this int value, int minimum, int maximum) {
            return value >= minimum && value <= maximum;
        }

        static void Main(string[] args) {
            var input = ParseInput(args[0]);

            var enumerable = input as Dictionary<string, string>[] ?? input.ToArray();
            Console.WriteLine(
                $"Solving with input: \n{string.Join("\n", enumerable.Select(x => string.Join(" ", x.Select(y => $"{y.Key}:{y.Value}"))))}");
            Console.WriteLine($"Part 1 result: {Solve1(enumerable).ToString()}");
            Console.WriteLine($"Part 2 result: {Solve2(enumerable).ToString()}");
        }

        static int Solve1(IEnumerable<Dictionary<string, string>> input) {
            return input.Count(dataset =>
                dataset.ContainsKey("byr") && dataset.ContainsKey("iyr") && dataset.ContainsKey("eyr") &&
                dataset.ContainsKey("hgt") && dataset.ContainsKey("hcl") && dataset.ContainsKey("ecl") &&
                dataset.ContainsKey("pid"));
        }

        static int Solve2(IEnumerable<Dictionary<string, string>> input) {
            var byrRegex = new Regex(@"\d{4}");
            const int byrMin = 1920;
            const int byrMax = 2002;

            var iyrRegex = byrRegex;
            const int iyrMin = 2010;
            const int iyrMax = 2020;

            var eyrRegex = byrRegex;
            const int eyrMin = 2020;
            const int eyrMax = 2030;

            var hgtRegexIn = new Regex(@"(\d+)(?:in)");
            const int hgtInMin = 59;
            const int hgtInMax = 76;

            var hgtRegexCm = new Regex(@"(\d+)(?:cm)");
            const int hgtCmMin = 150;
            const int hgtCmMax = 193;

            var hclRegex = new Regex(@"#[0-9a-f]{6}");

            var eclRegex = new Regex(@"(amb)|(blu)|(brn)|(gry)|(grn)|(hzl)|(oth)");

            var pidRegex = new Regex(@"^\d{9}\b");

            return (from dataset in input
                where dataset.ContainsKey("byr") && dataset.ContainsKey("iyr") && dataset.ContainsKey("eyr") &&
                      dataset.ContainsKey("hgt") && dataset.ContainsKey("hcl") && dataset.ContainsKey("ecl") &&
                      dataset.ContainsKey("pid")
                where byrRegex.IsMatch(dataset["byr"]) && Convert.ToInt32(dataset["byr"]).IsWithin(byrMin, byrMax)
                where iyrRegex.IsMatch(dataset["iyr"]) && Convert.ToInt32(dataset["iyr"]).IsWithin(iyrMin, iyrMax)
                where eyrRegex.IsMatch(dataset["eyr"]) && Convert.ToInt32(dataset["eyr"]).IsWithin(eyrMin, eyrMax)
                where hgtRegexCm.Match(dataset["hgt"]).Success && Convert
                    .ToInt32(hgtRegexCm.Match(dataset["hgt"]).Groups[1].Value)
                    .IsWithin(hgtCmMin, hgtCmMax) || (hgtRegexIn.Match(dataset["hgt"]).Success && Convert
                    .ToInt32(hgtRegexIn.Match(dataset["hgt"]).Groups[1].Value)
                    .IsWithin(hgtInMin, hgtInMax))
                where hclRegex.IsMatch(dataset["hcl"])
                where eclRegex.IsMatch(dataset["ecl"])
                select dataset).Count(dataset => pidRegex.IsMatch(dataset["pid"]));
        }
    }
}